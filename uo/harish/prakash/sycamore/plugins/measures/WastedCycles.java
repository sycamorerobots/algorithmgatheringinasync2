/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.measures;

import java.util.Iterator;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.measures.MeasureImpl;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import uo.harish.prakash.sycamore.plugins.algorithms.GatheringInAsyncMeasureWastedCycle;

/**
 * @author harry
 *
 */
@PluginImplementation
public class WastedCycles extends MeasureImpl {

	private String	_author				= "Harish Prakash";
	private String	_pluginName			= "WastedCycles";
	private String	_shortDescription	= "Number of Wasted Cycles";
	private String	_description		= "Calculate the number of wasted cycles";

	@Override
	public void onSimulationStart() {}

	@Override
	public void onSimulationStep() {}

	@Override
	public void onSimulationEnd() {

		@SuppressWarnings("unchecked")
		Iterator<SycamoreRobot<Point2D>> robots = engine.getRobots().iterator();
		double average = 0;
		int numberOfActiveRobots = 0;

		while (robots.hasNext()) {
			SycamoreRobot<Point2D> robot = robots.next();
			if (GatheringInAsyncMeasureWastedCycle.class.isAssignableFrom(robot.getAlgorithm().getClass())) {
				GatheringInAsyncMeasureWastedCycle algorithm = GatheringInAsyncMeasureWastedCycle.class.cast(robot.getAlgorithm());
				average += (algorithm.getWastedCycles() / algorithm.getTotalCycles());
				numberOfActiveRobots += 1;
			}
		}

		average = average / numberOfActiveRobots;
		System.out.println(average);
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

}
