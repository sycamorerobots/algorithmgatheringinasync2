/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.util.ArrayList;
import java.util.Vector;
import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.gui.SycamoreSystem;

/**
 * @author harry
 *
 */
public class GatheringInAsyncUtil {

	public static ArrayList<Point2D> extractRobotLocations(Point2D callerPosition, Vector<Observation<Point2D>> observations) {

		ArrayList<Point2D> robotLocations = new ArrayList<>();
		for (Observation<Point2D> observation : observations) {

			Point2D robotPosition = observation.getRobotPosition();
			if (callerPosition.differsModuloEpsilon(robotPosition)) {

				boolean isNewPosition = true;

				for (Point2D oldPosition : robotLocations) {
					if (!oldPosition.differsModuloEpsilon(robotPosition)) {
						isNewPosition = false;
						break;
					}
				}

				if (isNewPosition) {
					robotLocations.add(robotPosition);
				}
			}
		}

		return robotLocations;
	}

	public static Point2D getClosestRightRobot(Point2D callerPosition, ArrayList<Point2D> snapshot) {

		Point2D closest = null;
		float distanceToClosest = Float.MAX_VALUE;

		for (Point2D robotPosition : snapshot) {
			float distanceToRobot = callerPosition.distanceTo(robotPosition);
			if (distanceToRobot < distanceToClosest && robotPosition.x - callerPosition.x > 2 * SycamoreSystem.getEpsilon()) {
				closest = robotPosition;
				distanceToClosest = distanceToRobot;
			}
		}

		return closest;
	}

	public static Point2D getClosestDownRobot(Point2D callerPosition, ArrayList<Point2D> snapshot) {

		Point2D closest = null;
		float distanceToClosest = Float.MAX_VALUE;

		for (Point2D robotPosition : snapshot) {
			float distanceToRobot = callerPosition.distanceTo(robotPosition);
			if (distanceToRobot < distanceToClosest) {
				if (callerPosition.y - robotPosition.y > SycamoreSystem.getEpsilon()) {
					if ((robotPosition.x <= callerPosition.x + SycamoreSystem.getEpsilon())) {

						closest = robotPosition;
						distanceToClosest = distanceToRobot;
					}
				}
			}
		}

		return closest;
	}

	public static boolean isThereLeftRobot(Point2D callerPosition, ArrayList<Point2D> snapshot) {

		for (Point2D robotPosition : snapshot) {
			if (callerPosition.x - robotPosition.x > 2 * SycamoreSystem.getEpsilon()) {
				return true;
			}
		}

		return false;
	}

	public static boolean isThereUpRobot(Point2D callerPosition, ArrayList<Point2D> snapshot) {

		for (Point2D robotPosition : snapshot) {
			if (robotPosition.y - callerPosition.y > SycamoreSystem.getEpsilon()) {
				if (robotPosition.x <= callerPosition.x + SycamoreSystem.getEpsilon()) {

					return true;
				}
			}
		}

		return false;
	}

	public static boolean isThereObstructingRobot(Point2D callerPosition, ArrayList<Point2D> snapshot) {

		return (isThereLeftRobot(callerPosition, snapshot) || isThereUpRobot(callerPosition, snapshot));
	}

	public static Point2D getUpperIntersectionOfCircle(Point2D callerPosition, Point2D robotPosition, float radius) {

		float horizontalDistance = robotPosition.x - callerPosition.x;

		Point2D intersection = new Point2D();
		intersection.x = robotPosition.x;
		intersection.y = callerPosition.y + (float) Math.sqrt(radius * radius - horizontalDistance * horizontalDistance);

		return intersection;
	}

	public static Point2D getDiagonalDestination(Point2D callerPosition, Point2D robotPosition, float radius) {

		// Algorithm Variable - B
		Point2D upperIntersection = getUpperIntersectionOfCircle(callerPosition, robotPosition, radius);
		// Algorithm Variable - A
		Point2D lowerPoint = new Point2D(callerPosition.x, callerPosition.y - radius);

		Point2D SB = VectorUtils.getVector(callerPosition, upperIntersection);
		Point2D SA = VectorUtils.getVector(callerPosition, lowerPoint);

		double cos = VectorUtils.dotProduct(SA, SB) / (VectorUtils.magnitude(SA) * VectorUtils.magnitude(SB));
		double angle = Math.toDegrees(Math.acos(cos));

		if (angle / 2 < 60) {
			robotPosition.x -= SycamoreSystem.getEpsilon();
			return getDiagonalDestination(callerPosition, robotPosition, radius);
		}

		else {
			return new Point2D(upperIntersection.x, upperIntersection.y - radius);
		}
	}
}
