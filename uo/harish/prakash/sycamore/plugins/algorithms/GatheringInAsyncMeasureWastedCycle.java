/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.util.Vector;
import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreObservedRobot;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class GatheringInAsyncMeasureWastedCycle extends GatheringInAsync {

	private double	_wastedCycles	= 0;
	private double	_totalCycles	= 0;

	@Override
	public Point2D compute(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		Point2D computed = super.compute(observations, caller);
		_totalCycles += 1;
		if (computed == null) {
			_wastedCycles += 1;
		}

		return computed;
	}

	@Override
	public String getPluginName() {

		return String.format("%sWC", super.getPluginName());
	}

	/**
	 * @return the wastedCycles
	 */
	public double getWastedCycles() {

		return _wastedCycles;
	}

	/**
	 * @return the totalCycles
	 */
	public double getTotalCycles() {

		return _totalCycles;
	}
}
