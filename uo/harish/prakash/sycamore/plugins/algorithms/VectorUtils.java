/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import it.diunipi.volpi.sycamore.engine.Point2D;

/**
 * @author harry
 *
 */
public class VectorUtils {

	public static Point2D getVector(Point2D fromPoint, Point2D toPoint) {

		Point2D vector = new Point2D();
		vector.x = toPoint.x - fromPoint.x;
		vector.y = toPoint.y - fromPoint.y;

		return vector;
	}

	public static float dotProduct(Point2D vector1, Point2D vector2) {

		return vector1.x * vector2.x + vector1.y * vector2.y;
	}

	public static double magnitude(Point2D vector) {

		return Math.sqrt((vector.x * vector.x) + (vector.y * vector.y));
	}
}
