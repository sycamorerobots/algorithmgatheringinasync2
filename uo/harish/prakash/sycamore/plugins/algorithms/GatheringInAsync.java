/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.util.ArrayList;
import java.util.Vector;
import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreObservedRobot;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.algorithms.AlgorithmImpl;
import it.diunipi.volpi.sycamore.plugins.visibilities.VisibilityImpl;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */

@PluginImplementation
public class GatheringInAsync extends AlgorithmImpl<Point2D> {

	private String	_author				= "Harish Prakash";
	private String	_shortDescription	= "Gathering In Asynchronous Scheduler";
	private String	_description		= "Robots gathering in asynchronous scheduler and consistent compss";
	private String	_pluginName			= "GatheringInAsync";
	private TYPE	_pluginType			= TYPE.TYPE_2D;

	private Long _terminateAt;

	@Override
	public void init(SycamoreObservedRobot<Point2D> robot) {

		_terminateAt = null;
	}

	@Override
	public Point2D compute(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		if (_terminateAt == null) {
			_terminateAt = System.currentTimeMillis() + 600000L;
		}

		else if (_terminateAt < System.currentTimeMillis()) {
			_terminateAt = null;
			setFinished(true);
			return null;
		}

		Point2D callerPosition = caller.getLocalPosition();
		ArrayList<Point2D> snapshot = GatheringInAsyncUtil.extractRobotLocations(callerPosition, observations);

		Point2D rightRobot = GatheringInAsyncUtil.getClosestRightRobot(callerPosition, snapshot);
		Point2D downRobot = GatheringInAsyncUtil.getClosestDownRobot(callerPosition, snapshot);
		boolean amObstructed = GatheringInAsyncUtil.isThereObstructingRobot(callerPosition, snapshot);

		if (rightRobot == null && downRobot == null && !amObstructed) {
			setFinished(true);
			return DoNothing();
		}

		else if (amObstructed) {
			return DoNothing();
		}

		else if (downRobot == null && rightRobot != null) {
			return HorizontalMove(caller, rightRobot);
		}

		else if (downRobot != null && rightRobot == null) {
			return VerticalMove(caller, downRobot);
		}

		else {
			return DiagonalMove(caller, rightRobot);
		}
	}

	private Point2D DoNothing() {

		return null;
	}

	private Point2D HorizontalMove(SycamoreObservedRobot<Point2D> caller, Point2D rightRobot) {

		Point2D horizontal = new Point2D(rightRobot.x, caller.getLocalPosition().y);
		caller.setDirection(horizontal);
		return horizontal;
	}

	private Point2D VerticalMove(SycamoreObservedRobot<Point2D> caller, Point2D downRobot) {

		caller.setDirection(downRobot);
		return downRobot;
	}

	private Point2D DiagonalMove(SycamoreObservedRobot<Point2D> caller, Point2D rightRobot) {

		float radius = VisibilityImpl.getVisibilityRange() / 2;
		Point2D diagonal = GatheringInAsyncUtil.getDiagonalDestination(caller.getLocalPosition(), rightRobot, radius);
		caller.setDirection(diagonal);
		return diagonal;
	}

	@Override
	public String getReferences() {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TYPE getType() {

		return _pluginType;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

}
